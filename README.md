一、支持solr集群

二、支持多个solr服务同时使用

三、支持一键高亮显示

四、动态切换排序字段、查询运算符、查询关键字

五、支持用户自定操作

六、16.5kb 大小 没有负担

七、快到没朋友

以下是代码实例





package ld.solr;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;

public class SolrTest {
	private static Logger log = Logger.getLogger(SolrTest.class);

	private static SolrKit solrKit;
	private static SolrClient client;
	private static SolrPlugin solrPlugin;

	/**
	 * 
	 *<p> 作用 : 加载solr 初始化组件</p>
	 *<p> 参数：</p>
	 *<p> 作者：Administrator-zxw</p>
	 *<p> 时间：2016年11月4日下午3:20:36</p>
	 */
	public static void start(){
		try {
			solrPlugin = new SolrPlugin("http://localhost:8080/solr/test");
			//高亮支持  默认false 标签默认 <font color='red'></font>
			solrPlugin.setHighlight(true);
			solrPlugin.setSimplePre("<a>");
			solrPlugin.setSimplePost("</a>");

			solrPlugin.start();
			solrKit = solrPlugin.getSolrKit();
			client = solrPlugin.getClient();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("启动solr");
	}
	public static void main(String[] args) throws Exception {

		//启动solr
		start();


		/**#################### 创建索引  ##########################**/

		//创建索引 第一种方法
		List<HashMap> list = new ArrayList<>();
		solrKit.createIndex(list);
		//创建索引 第二种方法
		SolrInputDocument doc = new SolrInputDocument();
		doc.addField("id", 1);
		client.add(doc);
		client.commit();


		/**#################### 查询索引  ##########################**/

		//查询索引 第一种方法 查询所有
		QueryResponse queryList1 = solrKit.queryList("要查询的值");
		//查询索引 第二种方法 分页查询
		QueryResponse queryList2 = solrKit.queryList("要查询的值",0,10);


		/**#################### 删除索引  ##########################**/

		//删除索引 第一种方法 删除所有
		solrKit.deleteAll();
		//删除索引 第二种方法 根据查询结果删除
		solrKit.deleteByQuery("要删除的值");

		/**#################### 判断索引  ##########################**/

		//判断是否存在索引
		solrKit.hasIndex();
	}

}
