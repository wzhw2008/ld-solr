package ld.solr;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




public class ObjcetUtil {
	/**
	 *  
	 *<p> 作用 :根据属性名获取属性值  </p>
	 *<p> 参数：@param fieldName
	 *<p> 参数：@param o
	 *<p> 参数：@return</p>
	 *<p> 作者：Administrator-zxw</p>
	 *<p> 时间：2016年8月26日下午6:08:10</p>
	 */
	public Object getFieldValueByName(String fieldName, Object o) {  
		try {    
			String firstLetter = fieldName.substring(0, 1).toUpperCase();    
			String getter = "get" + firstLetter + fieldName.substring(1);    
			Method method = o.getClass().getMethod(getter, new Class[] {});    
			Object value = method.invoke(o, new Object[] {});    
			return value;    
		} catch (Exception e) {    
			//	           log.error(e.getMessage(),e);    
			return null;    
		}    
	}   

	/**
	 *  
	 *<p> 作用 : 获取属性名数组 </p>
	 *<p> 参数：@param o
	 *<p> 参数：@return</p>
	 *<p> 作者：Administrator-zxw</p>
	 *<p> 时间：2016年8月26日下午6:08:00</p>
	 */
	public String[] getFiledName(Object o){  
		Field[] fields = o.getClass().getDeclaredFields();  
		String[] fieldNames=new String[fields.length];  
		for(int i=0;i<fields.length;i++){  
			System.out.println(fields[i].getType());  
			fieldNames[i]=fields[i].getName();  
		}  
		return fieldNames;  
	}  

	/**
	 * 获取属性类型(type)，属性名(name)，属性值(value)的map组成的list 
	 *<p> 作用 : </p>
	 *<p> 参数：@param o
	 *<p> 参数：@return</p>
	 *<p> 作者：Administrator-zxw</p>
	 *<p> 时间：2016年8月26日下午6:07:49</p>
	 */
	public List<Map<String,Object>> getFiledsInfo(Object o){  
		Field[] fields=o.getClass().getDeclaredFields();  
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();  
		Map<String,Object> infoMap=null;  
		for(int i=0;i<fields.length;i++){  
			infoMap = new HashMap<String,Object>();  
			infoMap.put("type", fields[i].getType().toString());  
			infoMap.put("name", fields[i].getName());  
			infoMap.put("value", getFieldValueByName(fields[i].getName(), o));  
			list.add(infoMap);  
		}  
		return list;  
	}  
	/**
	 * 
	 *<p> 作用 :获取对象的所有属性值，返回一个对象数组  </p>
	 *<p> 参数：@param o
	 *<p> 参数：@return</p>
	 *<p> 作者：Administrator-zxw</p>
	 *<p> 时间：2016年8月26日下午6:07:33</p>
	 */
	public Object[] getFiledValues(Object o){  
		String[] fieldNames=this.getFiledName(o);  
		Object[] value=new Object[fieldNames.length];  
		for(int i=0;i<fieldNames.length;i++){  
			value[i]=this.getFieldValueByName(fieldNames[i], o);  
		}  
		return value;  
	}

	/**
	 * 
	 *<p> 作用 :Map 转换 java bin </p>
	 *<p> 参数：@param type
	 *<p> 参数：@param map
	 *<p> 参数：@return
	 *<p> 参数：@throws IntrospectionException
	 *<p> 参数：@throws IllegalAccessException
	 *<p> 参数：@throws InstantiationException
	 *<p> 参数：@throws InvocationTargetException</p>
	 *<p> 作者：Administrator-zxw</p>
	 *<p> 时间：2016年8月26日下午6:06:34</p>
	 */
	public static Object convertMap(Class type, Map map)
			throws IntrospectionException, IllegalAccessException,
			InstantiationException, InvocationTargetException {
		BeanInfo beanInfo = Introspector.getBeanInfo(type); // 获取类属性
		Object obj = type.newInstance(); // 创建 JavaBean 对象

		// 给 JavaBean 对象的属性赋值
		PropertyDescriptor[] propertyDescriptors =  beanInfo.getPropertyDescriptors();
		for (int i = 0; i< propertyDescriptors.length; i++) {
			PropertyDescriptor descriptor = propertyDescriptors[i];
			String propertyName = descriptor.getName();

			if (map.containsKey(propertyName)) {
				// 下面一句可以 try 起来，这样当一个属性赋值失败的时候就不会影响其他属性赋值。
				Object value = map.get(propertyName);

				Object[] args = new Object[1];
				args[0] = value;

				descriptor.getWriteMethod().invoke(obj, args);
			}
		}
		return obj;
	}

}
