package ld.solr;

import java.util.List;

/**
 * 分页实体
 * @author 张现伟
 *
 */
public class PageInfo<T> {
	
	private List<T> list;
	
	private Integer page = 0;
	private Integer total = 0;
	private Integer pageSize = 10;

	public PageInfo() {
	}

	public PageInfo(Integer page, Integer pageSize) {
		this.page = page;
		this.pageSize = pageSize;
	}

	public PageInfo(Integer page, Integer pageSize, Integer total,List<T> list) {
		this.page = page;
		this.pageSize = pageSize;
		this.total = total;
		this.list = list;
	}

	/**
	 * 设置当前页号
	 * 
	 * @param page
	 *            当前页号
	 */
	public void setPage(Integer page) {
		this.page = page;
	}

	/**
	 * 设置每页包含的纪录数
	 * 
	 * @param pageSize
	 *            每页包含的纪录数
	 */
	public void setPageSize(Integer size) {
		this.pageSize = size;
	}

	/**
	 * 设置总记录数
	 * 
	 * @param total
	 *            总记录数
	 */
	public void setTotal(Integer total) {
		this.total = total;
	}

	/**
	 * 获取当前页号
	 * 
	 * @return 当前页号
	 */
	public Integer getPage() {
		return this.page;
	}

	/**
	 * 获取每页包含的纪录数
	 * 
	 * @return 每页包含的纪录数
	 */
	public Integer getPageSize() {
		return this.pageSize;
	}

	/**
	 * 获取总记录数
	 * 
	 * @return 总记录数
	 */
	public Integer getTotal() {
		return this.total;
	}

	/**
	 * 获取总页数
	 * 
	 * @return 总页数
	 */
	public int getTotalPage() {
		int tpage = total % pageSize;
		int totalpage = total / pageSize;
		if (tpage != 0) {
			totalpage += 1;
		}
		return totalpage;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}
	
}
