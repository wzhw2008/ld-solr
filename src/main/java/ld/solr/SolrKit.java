package ld.solr;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;



public class SolrKit{

	private static Logger lg = Logger.getLogger(SolrKit.class);
	private SolrClient client;
	private boolean highlight;
	private String simplePre;
	private String simplePost;

	public SolrKit(SolrClient client, boolean highlight, String simplePre, String simplePost) {
		this.client=client;
		this.highlight=highlight;
		this.simplePre=simplePre;
		this.simplePost=simplePost;
	}

	/**
	 * 
	 *<p> 作用 :创建索引 </p>
	 *<p> 参数：@param list
	 *<p> 参数：@throws SolrServerException
	 *<p> 参数：@throws IOException</p>
	 *<p> 作者：Administrator-zxw</p>
	 *<p> 时间：2016年11月3日下午3:43:55</p>
	 */
	public void createIndex(List<HashMap> list) throws SolrServerException, IOException{
		List<SolrInputDocument> docList = new ArrayList<SolrInputDocument>();
		for (int i = 0; i < list.size(); i++) {
			//建立存储对象
			SolrInputDocument doc = new SolrInputDocument();
			//遍历map
			Iterator<?> entries = list.get(i).entrySet().iterator();  
			while (entries.hasNext()) {  
				Map.Entry entry = (Map.Entry) entries.next();  
				String key = entry.getKey().toString();  
				String value = entry.getValue().toString();
				if(StringUtils.isNotEmpty(key) && StringUtils.isNotEmpty(value)){
					doc.addField(key,value);
				}
			}
			docList.add(doc);
		}
		client.add(docList);
		client.commit();
	};

	/**
	 * 
	 *<p> 作用 : 分页查询</p>
	 *<p> 参数：@param key
	 *<p> 参数：@param pageNum
	 *<p> 参数：@param pageSize
	 *<p> 参数：@return
	 *<p> 参数：@throws Exception</p>
	 *<p> 作者：Administrator-zxw</p>
	 *<p> 时间：2016年11月4日下午3:02:45</p>
	 */
	public QueryResponse queryList(String key,Integer pageNum, Integer pageSize) throws Exception{  
		//获取solr_config.xml
		List<Map<String, String>> xmlDem=null;
		try {
			xmlDem = SolrUtil.xmlDem("D:\\eclispe\\solr-6.1.0\\src\\main\\resources\\solr_config.xml");
		} catch (Exception e) {
			lg.error("没有获取到solr_config.xml 配置文件", e);
		}
		//创建solr查询对象
		SolrQuery query = new SolrQuery();
		//拼接查询sql
		StringBuilder sb = new StringBuilder();
		//开启高亮功能&高亮样式
		if(highlight){
			query.setHighlight(true);
			query.setHighlightSimplePre(simplePre);
			query.setHighlightSimplePost(simplePost);
		}
		//便利所有字段
		for (Map<String,String> map : xmlDem) {
			//isQuery = y  则查询、反之不查
			if(map.get("isQuery").equals("y")){
				//高亮字段
				query.addHighlightField(map.get("name"));
				sb.append(map.get("name")).append(":");
				//queryType 检索运算符 n代表默认查询 反之 使用设置的 检索运算符
				String queryType = map.get("queryType");
				if(queryType.equals("n")){
					sb.append(key);
				}else{
					sb.append(key+queryType);
				}
				sb.append(" OR ");
			}
			//isSort 排序字段 n 代表不排序  yd 排序并且是desc 、ya 排序并且是asc
			if(!map.get("isSort").equals("n")){
				if(map.get("isSort").equals("yd")){
					query.setSort(map.get("name"), SolrQuery.ORDER.desc);
				}else if(map.get("isSort").equals("ya")){
					query.setSort(map.get("name"), SolrQuery.ORDER.asc);
				}
			}
		}
		//减去拼接后三位
		String querys = sb.toString();
		if(querys.length() > 0){
			querys= querys.substring(0, querys.length()-3);
		}

		//分页规则 不等于空 分页  等于空   不分页
		if(pageSize != null && pageNum != null){
			query.setStart(pageNum*pageSize);//起始页数
			query.setRows(pageSize);//每页总条数
		}else{
			query.set("start", 0);
			query.set("rows", Integer.MAX_VALUE);
		}
		//放入查询对象
		query.setQuery(querys);
		//打印查询sql
		lg.info("solr query key :"+query);
		//返回结果集
		return client.query(query);
	}

	/**
	 * 
	 *<p> 作用 : 查询所有</p>
	 *<p> 参数：@param key
	 *<p> 参数：@return
	 *<p> 参数：@throws Exception</p>
	 *<p> 作者：Administrator-zxw</p>
	 *<p> 时间：2016年11月4日下午3:02:24</p>
	 */
	public QueryResponse queryList(String key) throws Exception{  
		//获取solr_config.xml
		List<Map<String, String>> xmlDem=null;
		try {
			xmlDem = SolrUtil.xmlDem("D:\\eclispe\\solr-6.1.0\\src\\main\\resources\\solr_config.xml");
		} catch (Exception e) {
			lg.error("没有获取到solr_config.xml 配置文件", e);
		}
		//创建solr查询对象
		SolrQuery query = new SolrQuery();
		//拼接查询sql
		StringBuilder sb = new StringBuilder();
		//开启高亮功能&高亮样式
		if(highlight){
			query.setHighlight(true);
			query.setHighlightSimplePre(simplePre);
			query.setHighlightSimplePost(simplePost);
		}
		//便利所有字段
		for (Map<String,String> map : xmlDem) {
			//isQuery = y  则查询、反之不查
			if(map.get("isQuery").equals("y")){
				//高亮字段
				query.addHighlightField(map.get("name"));
				sb.append(map.get("name")).append(":");
				//queryType 检索运算符 n代表默认查询 反之 使用设置的 检索运算符
				String queryType = map.get("queryType");
				if(queryType.equals("n")){
					sb.append(key);
				}else{
					sb.append(key+queryType);
				}
				sb.append(" OR ");
			}
			//isSort 排序字段 n 代表不排序  yd 排序并且是desc 、ya 排序并且是asc
			if(!map.get("isSort").equals("n")){
				if(map.get("isSort").equals("yd")){
					query.setSort(map.get("name"), SolrQuery.ORDER.desc);
				}else if(map.get("isSort").equals("ya")){
					query.setSort(map.get("name"), SolrQuery.ORDER.asc);
				}
			}
		}
		//减去拼接后三位
		String querys = sb.toString();
		if(querys.length() > 0){
			querys= querys.substring(0, querys.length()-3);
		}
		query.set("start", 0);
		query.set("rows", Integer.MAX_VALUE);
		//放入查询对象
		query.setQuery(querys);
		//打印查询sql
		lg.info("solr query key :"+query);
		//返回结果集
		return client.query(query);
	}
	/**
	 * 
	 *<p> 作用 : 根据索引删除 实例 "id:1"</p>
	 *<p> 参数：@param query
	 *<p> 参数：@throws SolrServerException
	 *<p> 参数：@throws IOException</p>
	 *<p> 作者：Administrator-zxw</p>
	 *<p> 时间：2016年8月29日下午3:17:37</p>
	 */
	public void deleteByQuery(String query) throws SolrServerException, IOException{  
		client.deleteByQuery(query);  
		client.commit(false, false);  
	}
	/**
	 * @throws IOException  
	 * @Description: 判断是否有索引数据 
	 * @author kang 
	 * @创建时间 2015下午4:54:24 
	 * @param @param keyword 
	 * @param @param pageNum 
	 * @param @param pageSize 
	 * @param @param clzz 
	 * @param @return 
	 * @return PageInfo<T> 
	 * @throws 
	 */  
	public boolean hasIndex() throws Exception{  
		SolrQuery query = new SolrQuery();  
		query.setQuery("*:*")// 查询内容  
		.setStart(0)// 分页  
		.setRows(1);  
		QueryResponse response = null;  
		response = client.query(query);  
		// 总记录数  
		int total = new Long(response.getResults().getNumFound()).intValue();  
		return total == 0 ? false : true;  
	}  
	/** 
	 *  
	 * @Description: 删除所有 
	 * @author kang 
	 * @创建时间 2015下午5:36:21 
	 * @param @throws SolrServerException 
	 * @param @throws IOException 
	 * @return void 
	 * @throws 
	 */  
	public void deleteAll() throws SolrServerException, IOException{  
		client.deleteByQuery("*:*");  
		client.commit(false, false);  
	}

}