package ld.solr;

import java.net.MalformedURLException;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.LBHttpSolrClient;

/**
 * 
 *<p>标题:solrPlugin </p>
 *<p>描述: solr 启动与关闭</p>
 *<p>单位:大家智合(北京)网络科技股份有限公司 </p>
 *<p>作者: Administrator--zxw</p>
 *<p>时间: 2016年11月3日下午3:28:58 </p>
 */
public class SolrPlugin{

	private static Logger lg = Logger.getLogger(SolrPlugin.class);

	private static String URL=null;
	private SolrClient client;
	private boolean Highlight=false;
	private String SimplePre="<font color='red'>";
	private String SimplePost="</font>";
	private SolrKit solrKit;

	public SolrClient getClient() {
		return client;
	}
	public SolrKit getSolrKit() {
		return solrKit;
	}
	public boolean isHighlight() {
		return Highlight;
	}

	public void setHighlight(boolean highlight) {
		Highlight = highlight;
	}

	public String getSimplePre() {
		return SimplePre;
	}

	public void setSimplePre(String simplePre) {
		SimplePre = simplePre;
	}

	public String getSimplePost() {
		return SimplePost;
	}

	public void setSimplePost(String simplePost) {
		SimplePost = simplePost;
	}

	/**
	 * 格式  url,url,url
	 * @param url
	 * @throws MalformedURLException 
	 */
	public SolrPlugin(String url) throws MalformedURLException {
		URL=url;
	}

	@SuppressWarnings("deprecation")
	public boolean start() {
		try {
			client=new LBHttpSolrClient(URL);
			solrKit=new SolrKit(client,Highlight,SimplePre,SimplePost);
			lg.info("Solr plug-in to start successfully");
		} catch (Exception e) {
			lg.error("Solr plug-in startup failed", e);
		}
		return true;
	}

	public boolean stop() {
		try {
			client.close();
			lg.info("Solr plug-in closure success");
		} catch (Exception e) {
			lg.error("Solr plug-in shutdown failed", e);
		}
		return true;
	}  
}
