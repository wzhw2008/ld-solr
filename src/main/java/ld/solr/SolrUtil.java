package ld.solr;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class SolrUtil {
	/**
	 * 
	 *<p> 作用 :xml 数据配置文件解析 </p>
	 *<p> 参数：@param conf
	 *<p> 参数：@return</p>
	 *<p> 作者：Administrator-zxw</p>
	 *<p> 时间：2016年9月29日下午11:45:59</p>
	 * @throws DocumentException 
	 */
	public static List<Map<String, String>> xmlDem(String conf) throws DocumentException{
		SAXReader saxReader = new SAXReader();
		File file = new File(conf);
		org.dom4j.Document read;
		List<Map<String, String>> list=new ArrayList<>();
		read = saxReader.read(file);
		List<Element> elements = read.getRootElement().elements();
		for (Element element : elements) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("name", element.attributeValue("name"));
			map.put("type", element.attributeValue("type"));
			map.put("isQuery", element.attributeValue("isQuery"));
			map.put("isSort", element.attributeValue("isSort"));
			map.put("queryType", element.attributeValue("queryType"));
			list.add(map);
		}
		return list;
	}

}
